from ping3 import ping
from time import sleep
import logging
import os

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='[%(asctime)s - '+os.path.basename(__file__)+' - %(levelname)s] %(message)s',datefmt='%Y-%m-%d %H:%M:%S')
    host_list = [
        'mobile-6206-cna.cern.ch',
        'mobile-6744-cna.cern.ch'
    ]
    while True:
        for host in host_list:
            if delay := ping(host):
                logging.info(f'{host} is online, {delay} ms')
            else:
                logging.info(f'{host} is offline')
            sleep(5)